echo "enter domain (next. will be appended)"
read read_domain
echo "enter ip of this server"
read read_ip
echo "enter username"
read read_user
stty -echo
echo "enter password"
read read_pass
echo "confirm password"
read read_confirm
stty echo
if [ "$read_pass" != "$read_confirm" ]
then
    echo "passwords do not match."
    exit
fi
echo "domain=$read_domain" > .env
echo "ip=$read_ip" >> .env
echo "user=$read_user" >> .env
echo "pass=$read_pass" >> .env
docker-compose up -d

sudo sed -i "s?0 => 'localhost',?0 => 'next.$read_domain', 1 => '$read_ip',?g" /raid/nextcloud/config/config.php