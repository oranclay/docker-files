if [ -z "$1"]
then
    echo "enter password:"
    stty -echo
    read jared_passwd
    echo "verify password:"
    read verify
    stty echo
    if [ "$verify" != "$jared_passwd" ]
    then
        exit
    fi
else
    jared_passwd=$1
fi

filename="docker-startup-script"
while IFS= read -r line
do
    if [[ $line = *$()* ]]
    then
        new_line=$(echo $line | sed "s?()?$jared_passwd?g")
        echo $new_line >> docker-startup-script.sh
    else
        echo $line >> docker-startup-script.sh
    fi
done < $filename

docker-compose up -d

rm docker-startup-script.sh

docker exec remote-shell service ssh start